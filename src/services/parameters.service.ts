import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ParametersService {

  public parameterToBePassed;

  constructor() {}

  setParam(parameter) {
    this.parameterToBePassed = parameter;
  }

  getParam() {
    let temp = this.parameterToBePassed;
    this.parameterToBePassed = null;
    return temp;
  }
}
