import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toast: ToastController) {}

  async showToast(message: string, duration: number) {
    const toast = await this.toast.create({
      message: message,
      duration: duration
    });
    toast.present();
  }
}
