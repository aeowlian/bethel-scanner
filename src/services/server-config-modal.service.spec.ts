import { TestBed } from '@angular/core/testing';

import { ServerConfigModalService } from './server-config-modal.service';

describe('ServerConfigModalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServerConfigModalService = TestBed.get(ServerConfigModalService);
    expect(service).toBeTruthy();
  });
});
