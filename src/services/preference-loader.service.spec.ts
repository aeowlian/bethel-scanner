import { TestBed } from '@angular/core/testing';

import { PreferenceLoaderService } from './preference-loader.service';

describe('PreferenceLoaderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PreferenceLoaderService = TestBed.get(PreferenceLoaderService);
    expect(service).toBeTruthy();
  });
});
