import { Injectable } from '@angular/core';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';

@Injectable({
  providedIn: 'root'
})
export class PreferenceLoaderService {

  static readonly TOKEN_KEY = "pq98wehfasdf";
  static readonly ADDRESS_KEY = "tquihflakjsf";
  static readonly USERNAME_KEY = "yapsdyfoquiwehjkr";
  static readonly UID_KEY = "56yhbvdrtyujdfdec";
  static readonly PASSWORD_KEY = "i3jenbdgcyiw";
  static readonly NEXT_KEY = "ksdhfaldhsflaiuh";
  private serverAddress;

  constructor(private appPreference: AppPreferences) { }

  setToken(token: string) {
    this.appPreference.store(PreferenceLoaderService.TOKEN_KEY, token);
  }

  getToken(): Promise<any> {
    return this.appPreference.fetch(PreferenceLoaderService.TOKEN_KEY);
  }

  setUserID(token: string) {
    this.appPreference.store(PreferenceLoaderService.UID_KEY, token);
  }

  getUserID(): Promise<any> {
    return this.appPreference.fetch(PreferenceLoaderService.UID_KEY);
  }

  setUsername(token: string) {
    this.appPreference.store(PreferenceLoaderService.USERNAME_KEY, token);
  }

  getUsername(): Promise<any>{
    return this.appPreference.fetch(PreferenceLoaderService.USERNAME_KEY);
  }

  get address() {
    return this.serverAddress;
  }

  setAddress(address: string) {
    if(address == null || address == '') return;
    this.appPreference.store(PreferenceLoaderService.ADDRESS_KEY, 'http://' + address + '/api/v1');
    this.serverAddress = 'http://' + address + '/api/v1';
  }

  getAddress(): Promise<any>{
    return this.appPreference.fetch(PreferenceLoaderService.ADDRESS_KEY);
  }

  setNext(token: boolean) {
    this.appPreference.store(PreferenceLoaderService.NEXT_KEY, token + "");
  }

  getNext(): Promise<any>{
    return this.appPreference.fetch(PreferenceLoaderService.NEXT_KEY);
  }

  // setPassword(password: string) {
  //   this.appPreference.store(PreferenceLoaderService.PASSWORD_KEY, password);
  // }

  // getPassword(): Promise<any>{
  //   return this.appPreference.fetch(PreferenceLoaderService.PASSWORD_KEY);
  // }

  clearAll() {
    this.appPreference.clearAll();
  }
}
