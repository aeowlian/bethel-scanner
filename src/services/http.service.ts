import { HttpClient } from '@angular/common/http';
import { PreferenceLoaderService } from './preference-loader.service';
import { Injectable } from '@angular/core';
import { ToastService } from './toast.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private errors: String;

  constructor(
    private preferenceLoader: PreferenceLoaderService,
    private toast: ToastService,
    private http: HttpClient
  ) { }

  testGet(): String {
    this.http.get('http://192.168.43.131:8000/api/v1/cart/1')
    .subscribe(
      response => {
        console.log(response['data']);
        this.errors = response['data'];
        return response['data'];
      },
      error => {
        console.log(error);
        this.errors = error;
        return error;
      }
    );
    // .catch(error => {
    //   this.errors = error.error + "\n" + error.status + "\n" + error.headers;
    //   console.log(error);
    //   console.log(error.status);
    //   console.log(error.error); // error message as string
    //   console.log(error.headers);
    //   return error.error + "\n" + error.status + "\n" + error.headers;
    // });
    return this.errors;
  }

  get(param: string){
    let address = this.preferenceLoader.address;
    // this.toast.showToast(address + param, 1000);
    return this.http.get(address + param);
  }

  post(param: string, body: Object){
    let address = this.preferenceLoader.address;
    // this.toast.showToast(address + param, 1000);
    return this.http.post(address + param, body);
  }

  put(param: string, body: Object){
    let address = this.preferenceLoader.address;
    // this.toast.showToast(address + param, 1000);
    return this.http.put(address + param, body);
  }
}
