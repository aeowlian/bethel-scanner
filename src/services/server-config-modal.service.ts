import { PreferenceLoaderService } from './preference-loader.service';
import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ServerConfigModalService {

  private modal;
  test;

  constructor(private alertController: AlertController, private preferenceLoader: PreferenceLoaderService) {

  }

  async showModal() {
    if(this.modal == null){
      let serverAddress = await this.preferenceLoader.getAddress();
      this.modal = await this.alertController.create({
        header: 'Enter Server Address',
        message: 'Server Address belum di set',
        inputs: [
          {
            name: 'newServerAddress',
            type: 'text',
            placeholder: 'Server Address',
            value: serverAddress,
            id: 'newServerAddress'
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('Confirm Cancel');
            }
          }, {
            text: 'Set',
            handler: check => {
              console.log(serverAddress);
              this.test = check;
              this.preferenceLoader.setAddress(check.newServerAddress);
            }
          }
        ]
      });
    }

    await this.modal.present();
  }
}
