import { Component, OnInit } from '@angular/core';
import { ParametersService } from 'src/services/parameters.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-message',
  templateUrl: './message.page.html',
  styleUrls: ['./message.page.scss'],
})
export class MessagePage implements OnInit {

  message: String;

  constructor(private parameterService: ParametersService, private router: Router) { }

  ngOnInit() {
    this.message = this.parameterService.getParam();
  }

  ok() {
    this.router.navigateByUrl('/home');
  }
}
