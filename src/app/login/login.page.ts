import { ServerConfigModalService } from './../../services/server-config-modal.service';
import { HttpService } from './../../services/http.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ToastService } from 'src/services/toast.service';
import { PreferenceLoaderService } from 'src/services/preference-loader.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  serverAddress: string;
  username: string;
  password: string;

  constructor(
    private http: HttpService,
    private router: Router,
    private toast: ToastService,
    private preferenceLoader: PreferenceLoaderService,
    private serverConfigModal: ServerConfigModalService
  ) {}

  ngOnInit() {
    this.preferenceLoader.getAddress().then(address => {
      if(address == null){
        this.serverAddress = 'not configured';
      } else {
        this.serverAddress = address;
      }
    });
    if(this.username == null || this.username == ''){
      this.preferenceLoader.getUsername().then(username => {
        this.username = username;
      });
    }
  }

  async login() {
    this.serverAddress = await this.preferenceLoader.getAddress();
    if(this.serverAddress == 'not configured' || this.serverAddress == null){
      await this.serverConfigModal.showModal();
      this.serverAddress = await this.preferenceLoader.getAddress();
      return;
    }
    if(this.error == ''){ //if no error
      //try login
      let exampleToken = 'asdfasdfasdfasdfasdf';
      let exampleUID = '17';

      //then
      this.preferenceLoader.setUserID(exampleUID);
      this.preferenceLoader.setUsername(this.username);
      this.preferenceLoader.setToken(exampleToken);
      this.toast.showToast('Login success!', 5000);
      this.router.navigateByUrl('/home');
    }
    // this.toast.show('Login success!', '5000', 'center');
    // this.username = "clicked";
    // this.username = this.http.testGet();

    //contoh http get
    // this.http.get('/cart/1', {})
    // .then(result => {
    //   this.toast.showToast('Login success!', 5000);
    //   this.username = result.data;
    // });
  }

  get error(): string {
    if(this.username == null || this.username == ''){
      return 'Username kosong. Silahkan isi username terlebih dahulu.';
    }
    if(this.password == null || this.password == ''){
      return 'Password kosong. Silahkan isi password terlebih dahulu.';
    }
    return '';
  }
}
