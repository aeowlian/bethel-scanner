import { ParametersService } from './../../services/parameters.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastService } from 'src/services/toast.service';

@Component({
  selector: 'app-success',
  templateUrl: './success.page.html',
  styleUrls: ['./success.page.scss'],
})
export class SuccessPage implements OnInit {

  result;

  constructor(
    private parameterService: ParametersService,
    private router: Router,
    private toast: ToastService
  ) { }

  ngOnInit() {
    this.result = this.parameterService.getParam();
    this.result.main = this.result.data[0];
  }

  redeemTicket() {
    if(!this.validRedeem){
      this.parameterService.parameterToBePassed = "Voucher nomor " + this.result.main.idVoucher + " berhasil di-verify!";
      this.router.navigateByUrl('/message');
    }else{
      // this.toast.showToast("Silahkan arahkan ke customer service.", 8000).then(() => {
      //   this.toast.showToast("Voucher sudah tidak ditukarkan.", 3000);
      // });
    }
  }

  get validRedeem(): boolean {
    let isValid: boolean = false;
    for (let i = 0; i < this.result.data.length; i++) {
      if(this.result.data[i].statusVoucher == "redeemed"){
        isValid = true;
        break;
      }
    }
    return isValid;
  }

  get redeemable(): string {
    if(this.result == null || this.result.data == null){
      return 'Voucher tidak ditemukan';
    }
    if(this.validRedeem) {
      return 'Voucher tidak bisa ditukarkan, silahkan arahkan ke customer service.';
    }else{
      return '';
    }
  }

  cancel() {
    this.router.navigateByUrl('/home');
  }
}
