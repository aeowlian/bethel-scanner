import { PreferenceLoaderService } from './../../services/preference-loader.service';
import { ParametersService } from './../../services/parameters.service';
import { Component } from '@angular/core';
import { BarcodeScanner, BarcodeScanResult } from '@ionic-native/barcode-scanner/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {

  result: BarcodeScanResult;
  private next: boolean;

  constructor(
    private scanner: BarcodeScanner,
    private router: Router,
    private parameterService: ParametersService,
    private preferenceLoaderService: PreferenceLoaderService
  ) {}

  async scanBarcode() {
    this.scanner.scan().then(data => {
      console.log(data);
      this.result = data;
      this.scanBarcodeSuccess();
    }).catch(err => {
      console.log("Error: ", err);
    });
  }

  async scanBarcodeSuccess() {
    let next = await this.preferenceLoaderService.getNext();
    next = next * 1;
    let result;
    if(next == 1){
      result = {
        scanResult: this.result.text,
        data: [
          {
            name: "Jason",
            address: "kasturi",
            noKtp: "123456789",
            golDarah: "a",
            idVoucher: this.result.text,
            statusVoucher: "available"
          },
          {
            name: "Jason",
            address: "kasturi",
            noKtp: "123456789",
            golDarah: "a",
            idVoucher: "12263",
            statusVoucher: "redeemed"
          }
        ]
      };
    }else if(next == 2){
      result = {
        scanResult: this.result.text,
        data: [
          {
            name: "Jason",
            address: "kasturi",
            noKtp: "123456789",
            golDarah: "a",
            idVoucher: this.result.text,
            statusVoucher: "available"
          },
          {
            name: "Jason",
            address: "kasturi",
            noKtp: "123456789",
            golDarah: "a",
            idVoucher: "12263",
            statusVoucher: "available"
          }
        ]
      };
    }else{
      result = {
        scanResult: this.result.text,
        data: [
          {
            name: "Jason",
            address: "kasturi",
            noKtp: "123456789",
            golDarah: "a",
            idVoucher: this.result.text,
            statusVoucher: "available"
          }
        ]
      };
      next = 0;
    }
    this.preferenceLoaderService.setNext(next + 1);
    
    this.parameterService.parameterToBePassed = result;
    this.router.navigateByUrl('/success');
  }

  scanBarcodeFail() {
    this.parameterService.parameterToBePassed = "Fail message";
    this.router.navigateByUrl('/message');
  }

  logout() {
    this.preferenceLoaderService.clearAll();
    this.router.navigateByUrl('/');
  }

  ngOnInit() {
    console.log("at home page");
    // this.preferenceLoaderService.getUsername().then(username => {
    //   if(username == null || username == ''){
    //     this.router.navigateByUrl('/login');
    //   }
    // });
    // this.preferenceLoaderService.getPassword().then(password => {
    //   if(password == null || password == ''){
    //     this.router.navigateByUrl('/login');
    //   }
    // });
  }
}
